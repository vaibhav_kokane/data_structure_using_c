#include <stdio.h>
#include <stdlib.h>

struct array_adt
{
    int *A;
    int size;
    int length;
};

void display(struct array_adt arr) {
    int i;
    for(i = 0;i < arr.length;i++)
        printf("%d ",arr.A[i]);
}

int main() {
    int i,n;
    struct array_adt arr;
    arr.length = 0;

    printf("Enter size of array = ");
    scanf("%d",&arr.size);
    printf("\n");
   
    printf("enter number of elements you want insert in array = ");
    scanf("%d",&n);
    
    arr.A = (int *)malloc(n*sizeof(int));

    for(i = 0; i < n; i++) 
        scanf("%d",&arr.A[i]);
    arr.length = n;
    display(arr);
    return 0;
}