#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

struct Array {
    int A[10];
    int size;
    int length;
};

void display(struct Array arr) {
    int i;
    for (i = 0; i < arr.length; i++)
        printf("%d ",arr.A[i]);
    
}

void append(struct Array *arr,int x) {
    if(arr->length < arr->size)
        arr->A[arr->length++] = x;
}

void insert(struct Array *arr,int index,int x) {
    int i;
    if(index >= 0 && index <= arr->length)
        for(i = arr->length; i > index ; i--)
            arr->A[i] = arr->A[i-1];
        arr->A[index] = x;
        arr->length++;
}

void delete(struct Array *arr,int index) {
    int i;
    if (index >=0 && index <= arr->length)
        for(i = index; i < arr->length;i++)
            arr->A[i] = arr->A[i+1];
        arr->length--;
}

int linearSearchWhileLoop(struct Array *arr,int value) {
    int i=0;
    while( i < arr->length) {
        if(value == arr->A[i])
            return i;
        i++;
    }  
    return -1;
}

int linearSearchForLoop(struct Array *arr,int value) {
    int i;
    for(i = 0; i < arr->length; i++) {
        if(value == arr->A[i])
            return i;
    }
    return -1;
}

void swap(int *x, int *y) {
    *x = *x + *y;
    *y = *x - *y;
    *x = *x - *y;
}

int linearSearchTrnsposition(struct Array *arr, int value) {
 int i,j;
    for(i = 0; i < arr->length; i++) {
        if(value == arr->A[i]){
            swap(&arr->A[i],&arr->A[i-1]);
            return i;
        }
    }
    return -1;
}

int linearSearchMoveToHeadOrFront(struct Array *arr, int value) {
  int i,j;
    for(i = 0; i < arr->length; i++) {
        if(value == arr->A[i]){
            swap(&arr->A[i],&arr->A[0]);
            return i;
        }
    }
    return -1;
}

int binarySearch(struct Array arr, int value) {
    int low = 0;
    int high = arr.length;
    int mid = 0;
        while(low <= high) {
            mid = (low + high)/2;
            if(value == arr.A[mid])
                return mid;
            if(value < arr.A[mid])
                high = mid - 1;
            else if(value > arr.A[mid])   
                low = mid + 1;   
        }
        return -1;
}

int binarySearchRecursive(int A[], int low, int high, int value) {  
    int mid ;
    if (low <= high) {
        mid = (low+high)/2;
        if(value == A[mid])
            return mid;
        else if(value < A[mid])
            binarySearchRecursive(A, low, mid - 1, value);
        else 
            binarySearchRecursive(A, mid + 1, high, value);
    } else
    return -1;
}

int get(int a[], int length,int index) {
    if(index >= 0 && index < length)
        return a[index];
    return -1;
}

int set(struct Array *arr, int index,int value) {
    if(index >= 0 && index <= arr->length)
        arr->A[index] = value;
        return  arr->A[index];
    return -1;
}

int max(struct Array arr) {
    int i;
    int max = arr.A[0];
    for ( i = 0; i < arr.length; i++) {
        if(max < arr.A[i]+1)
            max = arr.A[i];
    }
    return arr.A[i];
}

int min(struct Array arr) {
    int i;
    int min = arr.A[0];
    for ( i = 0; i < arr.length; i++) {
        if(min > arr.A[i])
            min = arr.A[i];
    }
    return arr.A[i];
}

int sum(struct Array arr) {
    int sum = 0; int i;
    for(i = 0; i < arr.size; i++) {
        sum = sum + arr.A[i];
    }
    return sum;
}

int sumRecursive(int a[],int n) {
    if (n < 0) 
        return 0;
    else
        return sumRecursive(a,n-1) + a[n];      
}

int avg(struct Array arr) {
    int avg = sum(arr);
    return avg/arr.length;
}

void reverseUsingArray(struct Array *arr) {
    printf("before reverse");
    int b[10]; int i,j = 0;
    for (i = arr->length-1, j = 0; i >= 0; i--, j++)
         b[j] = arr->A[i];
    for (i = 0; i < arr->length; i++)
        arr->A[i] = b[i];
    
}

void reverseUsingSwap(struct Array *arr) {
    int i,j,temp;
    for (i = 0,j = (arr->length)-1; i < ceil((arr->length)/2); i++, j--) {
        temp = arr->A[i];
        arr->A[i] = arr->A[j];
        arr->A[j] = temp;
    }
}

void leftShift(struct Array *arr) {
    int i;
    for(i = 0; i < arr->length; i++) {
        arr->A[i] = arr->A[i+1];
    }
}

void rightShift(struct Array *arr) {
    int i;
    for(i = arr->length; i >= 0; i--) {
        arr->A[i] = arr->A[i-1];
        if(i == 0)
            arr->A[0] = 0;
    }
}

void leftRoation(struct Array *arr) {
    int i;
    int lastindex = arr->length-1;
    int firstvalue = arr->A[0];
    for(i = 0; i < arr->length; i++) {
            arr->A[i] = arr->A[i+1];
    }
    arr->A[lastindex] = firstvalue;
}

void rightRotation(struct Array *arr) {
    int i;
    int last = arr->A[arr->length-1];
    for(i = arr->length-1; i >=0; i--) {
            arr->A[i] = arr->A[i-1];
    }
    arr->A[0] = last;
}

int insertElementInSortedArray(struct Array *arr,int element) {
    int i = arr->length-1;
    if(arr->length == arr->size)
        return;
    while(arr->A[i] > element) {
        arr->A[i+1] = arr->A[i];
        i--;
    }
    arr->A[i+1] = element;
    arr->length++;
}

int isSorted(struct Array arr) {
    int i = 0;
    while(i < arr.length-1 ) {
        if(arr.A[i] > arr.A[i+1])
            return false;
    i++;
    }
    return true;
}

// negative on left side and positive on right side
void Rearrange(struct Array *arr) {
    int i = 0, j = arr->length-1;
    while(i < j) {
        while(arr->A[i] < 0)
            i++;
        while(arr->A[j] > 0)
            j--;
        if(i < j)
            swap(&arr->A[i],&arr->A[j]);
    }
}

void mergeSort(struct Array *arr) {
    int i = 0, j = 0, k = 0, m = 0, n = 0;
    int *c = (int *)malloc(15 * sizeof(int)); 
    int *b = (int *)malloc(7 * sizeof(int));
    b[0] = 2; b[1] = 4; b[2] = 6; b[3] = 8; b[4] = 10; b[5] = 12; b[6] = 14;
    m = arr->length; n =6;

    while(i < m && j < n) {
        if(arr->A[i] < b[j])
            c[k++] = arr->A[i++];
        if(arr->A[i] > b[j])
            c[k++] = b[j++];
    }

    for(;i<m;i++) {
        c[k++] = arr->A[i];
    }

    for(;j<n;j++) {
        c[k++] = b[j];
    }

    for (int i = 0; i < 15; i++)
        printf("%d ",c[i]);
}

int main() {
    struct Array arr = { {1,3,5,7,9}, 10, 5};
    // append(&arr,60);
    // insert(&arr,2,30);
    // delete(&arr,0);
    // printf("%d ",linearSearchWhileLoop(&arr,50));
    // printf("%d ",linearSearchForLoop(&arr,50));
    // printf("%d ",linearSearchTrnsposition(&arr,50));
    // printf("%d ",linearSearchMoveToHeadOrFront(&arr,50));
    // printf("%d ",binarySearch(arr,110));
    // printf("%d",binarySearchRecursive(arr.A,0,arr.length,10)); 
    // printf("%d ",get(arr.A, arr.length,12));
    // printf("%d ",set(&arr, 9, 110));
    // printf("%d ",max(arr));
    // printf("%d ",min(arr));
    // printf("%d ",sum(arr));
    // printf("%d ",sumRecursive(arr.A,arr.length));
    // printf("%d ",avg(arr));
    // reverseUsingArray(&arr);
    // reverseUsingSwap(&arr);
    // rightShift(&arr);
    // leftShift(&arr);
    // leftRoation(&arr);
    // rightRotation(&arr);
    // isSorted(&arr,15);
    // printf("%s",checkArrayIsSortedOrNot(arr) ? "True" : "False");
    // Rearrange(&arr);
    mergeSort(&arr);
    // display(arr);

    return 0;
} 
