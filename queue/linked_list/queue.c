#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Node {
    int data;
    struct Node *next;
}*front = NULL, *rear = NULL;

void enqueue(int data) {
    struct  Node *t;
    t = (struct Node *) malloc(sizeof(struct Node));
    if(t == NULL) printf("Queue is Full!\n");
    else {
        t->data = data;
        t->next = NULL;
        if(front == NULL) front = rear = t;
        else {
            rear->next = t;
            rear = t;
        }
    }
}

int dequeue() {
    struct Node *t; int x=0;
    if(front == NULL) printf("Queue is Empty!\n");
    else {
        t = (struct Node *)malloc(sizeof(struct Node));
        x = front->data;
        t = front ;
        front = front->next; 
        free(t);
    }
    return x;
}

void display() {
    struct Node *p = front;
    while(p) {
        printf("%d ",p->data);
        p = p->next;
    }
    printf("\n");
}

void main() {
    int x = 0;
    enqueue(10);
    enqueue(20);
    enqueue(30);
    enqueue(40);
    enqueue(50);
    display();
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    x = dequeue();
    printf("deleted element from queue = %d\n",x == 0 ? -1:x);
    display();
}