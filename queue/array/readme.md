<!-- Queue implementation using array --> 
- Queue follow FIFO approach i.e first in first out.
- Element is inserted in queue from back side of array and deleted from frond side.
- Queue can be created using two ways as below :
    1. Queue using Single pointer
        => Analysis :
                1. insert = O(1)
                2. delete = O(n)
        => Drawback :
                -drawback of single pointer that we have to shift all the elements to one position to left side (i.e towards front side) after deleting the first element of queue.
        => Solution :
                -Queue using two pointers.
    2. Queue using Two pointer's
        => Analysis :
                1. insert = O(1)
                2. delete = O(1)
        => Advantage :
                Constant time operation for both i.e insertion and deletion of the queue.
        => Drawback :
            1.can not use the space inside array of deleted element. in other words,every location used only once.
            2.there will be a point where queue is both empty and full at same time.
        => Solution : 
            1. Resetting pointers - this method does not guarantee the reuse of the deleted spaces by resetting pointers the  reason is that if the front and rear are never at same place we cannot reset them.
            2. Circular Queue - this is the best way to use queue in case of array.we can use spaces after delting also in circular manner.