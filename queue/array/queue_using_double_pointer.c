#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Queue{
    int size;
    int front;
    int rear;
    int *Q;
};

void create(struct Queue *q,int size) {
    q->size = size;
    q->front = q->rear = -1;
    q->Q = (int *)malloc(q->size * sizeof(int));
}

int isFull(struct Queue *q) {
    if(q->rear == q->size-1) return true;
    return false;
}

void enqueue(struct Queue *q, int data) {
    if(isFull(q)) printf("Queue is overflow!\n");
    else {
        q->rear++;
        q->Q[q->rear] = data;
    }
}

int isEmpty(struct Queue *q) {
    if(q->front == q->rear) return true;
    return false;
}

int dequeue(struct Queue *q) {
    int x = -1;
    if(isEmpty(q)) {
        printf("Queue is underflow!\n");
        return false;
    } else {
        q->front++;
        x = q->Q[q->front];
    }
    return x;
}

void display(struct Queue q) {
    int i;
    if(!isEmpty(&q))
        for (i = q.front+1; i <= q.rear; i++) printf("%d ",q.Q[i]);
    else 
        printf("Queue is empty!");
}

void main() {
struct Queue q;
int x,size;
printf("Enter the size of the Queue = ");
scanf("%d",&size);
create(&q,size);
enqueue(&q,10); enqueue(&q,20); enqueue(&q,30); enqueue(&q,40); enqueue(&q,50);
x = dequeue(&q);
printf("Element deleted from the Queue = %d\n",x == 0 ? -1:x);
display(q);
}