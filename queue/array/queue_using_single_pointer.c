#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Queue {
    // rear is index pointer not an address pointer
    int size;
    int rear;
    int *Q;
};

void create(struct Queue *q,int size) {
    q->rear = -1;
    q->size = size;
    q->Q = (int *) malloc(q->size * sizeof(int));
}

void enqueue(struct Queue *q, int data) {
    if(isFull(q)) {
        printf("Queue is overflow!\n");
    } else {
        q->rear++;
        q->Q[q->rear] = data;
    }
}

int dequeue(struct Queue *q) {
    if(isEmpty(q)) {
        return false;
    } else {
        int i,x;
        x = q->Q[0];
        for (i = 0; i < q->size; i++) {
            q->Q[i] = q->Q[i+1];
        }
        q->rear--;
        return x;
    }
}

void display (struct Queue q) {
    int i;
    for ( i = 0; i <= q.rear; i++) printf("%d ",q.Q[i]);
}

int isEmpty(struct Queue *q) {
    if(q->rear == -1) return true;
    return false;
}

int isFull(struct Queue *q) {
     if(q->rear == q->size-1) return true;
    return false;
}

int main() {
struct Queue q;
int size;
printf("Enter the size of Queue = ");
scanf("%d",&size);
create(&q,size); 
enqueue(&q,10); enqueue(&q,20); enqueue(&q,30); enqueue(&q,40); enqueue(&q,50);
enqueue(&q,60);
int x = dequeue(&q);
printf("deleted element from queue = %d\n",x == 0 ? -1:x);
display(q);
}