#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Queue {
    int size;
    // here front and rear are index pointer and not the address pointers.
    int front;
    int rear;
    int *Q;
};

void create(struct Queue *q,int size) {
    q->size = size;
    q->front = q->rear = 0;
    q->Q = (int *) malloc(q->size * sizeof(int));
}

int isFull(struct Queue *q) {
    if((q->rear+1)%(q->size) == q->front) return true;
    return false;
}

void enqueue(struct Queue *q,int data) {
    if(isFull(q)) printf("Circular queue is Full!\n");
    else {
        q->rear = (q->rear + 1) % q->size;
        q->Q[q->rear] = data;
    } 
}

int isEmpty(struct Queue *q) {
    if(q->front == q->rear) return true;
    return false;
}

int dequeue(struct Queue *q) {
    int x = -1;
    if(isEmpty(q)) printf("Circular queue is empty!\n");
    else {
        q->front = (q->front + 1) % q->size;
        x = q->Q[q->front];
    }
    return x;
}

void display(struct Queue q) {
    int i;
    for (i = q.front+1; i <= q.rear; i++) printf("%d ",q.Q[i]);
    printf("\n");
}

void main() {
    struct Queue q;
    int size,x;
    printf("Enter size of the circular queue = ");
    scanf("%d",&size);
    create(&q,size); 
    enqueue(&q,10); enqueue(&q,20); enqueue(&q,30); enqueue(&q,40); enqueue(&q,50); enqueue(&q,60);
    display(q);
    x = dequeue(&q);
    printf("Deleted element from circular queue = %d\n",x == 0 ? -1:x);
    display(q);
}