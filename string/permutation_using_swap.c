#include <stdio.h>
#include <string.h>

void swap(char str[],int l ,int i) {
    char temp = str[l];
    str[l] = str[i];
    str[i] = temp;
}

void permutation(char str[], int l, int h) {
    int i;
   if(l == h) {
        printf(" output = %s \n",str);
   } else {
        for ( i = l; i <= h; i++) {
            swap(str,l,i);
            permutation(str,l+1,h);
            swap(str,l,i);
        }
    }
}

void main() {
    char str[] = "abc";
     static int l,h;
    l = 0,h = strlen(str)-1;
    permutation(&str,l,h);
}