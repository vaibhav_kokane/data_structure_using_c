#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char * reverseStr(char *str, int len) {
    char *rev = (char *)malloc(10 * sizeof(char));
     int i,j;    
    for ( i = len-1, j = 0; i >= 0 ; i--, j++) {
        rev[j] = str[i];     
    }
    rev[j] = '\0';
    return rev;
}

void  reverseUsingSwap(char *str, int len) {
     int i,j;    
    for ( i = 0, j = len-1; i < j; i++, j--) {
        char *temp;
        temp =  str[i];
        str[i] =  str[j];
        str[j] =  temp;
    }
    printf("reverse = %s",str);

}

int main() {
    char str[10] = "vaibhav";
    int len = strlen(str);
   
    printf("string = %s reverse = %s \n",str,reverseStr(&str,len));
    // printf("string = %s \n",str);
    reverseUsingSwap(&str,len);
    
}