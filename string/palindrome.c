#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * reverseStr(char *str, int len) {
    char *rev = (char *)malloc(10 * sizeof(char));
     int i,j;    
    for ( i = len-1, j = 0; i >= 0 ; i--, j++) {
        rev[j] = str[i];     
    }
    rev[j] = '\0';
    return rev;
}

void compareString(char *s1, char *s2) {
    int i,j;
    if(strlen(s1) < strlen(s2))
        printf("smaller");
    if(strlen(s1) > strlen(s2))
        printf("greter");
    
    for ( i = 0,j = 0; s1[i] != '\0' && s2[i] != '\0'; i++,j++) {
        if(s1[i] != s2[j]) {
            printf("unequal");
            break;
        }
    }
    if(s1[i] == '\0' && s2[j] == '\0')
        printf("equal");
    
}


void checkPalindrome(char *str,int len) {
    char *str1 = (char *)malloc(6 * sizeof(char));
    str1 = reverseStr(str,len);
    compareString(str,str1);
}

int main() {
    char str[] = "madam";
    int len = strlen(str);
    checkPalindrome(&str,len);

    return 0;
}
