/*
    -> In Array we go left to right but in memory the byte will stored from right to  left.
    1.Left Shift - <<
    2.Bits ORing (Merging) -> setting the bit on in memory is merging
    3.Bits ANDing (Masking) -> by masking we will know the bit is on or off
    using this bitwise method we only find out duplication and value but  not the count of duplication.
*/ 

#include <stdio.h>

int main() {
    int i,x,h;
    char str[] = "vaibhav";
    h = 0;
    for (i = 0; str[i] != '\0' ; i++) {
        x = 1; 
        x = x << str[i] - 97;

        if(x & h) {
            printf("%c duplicate \n",str[i]);
        } else {
            h = x | h;
        }
    }   
}