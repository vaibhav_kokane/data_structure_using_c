/*
==================== ASCII Codes ( 0 to 127 (2^7) Ascii codes ) ============
1. Capital letters A = 65 to Z = 90; 2. Small letters   a = 97 to z = 122 
3. Numerical       0 = 48 to 9 = 57;

=========================== Unicodes ================================
1. unicode is for all the language and ascii code is the subset of unicode.
unicode is represed in hexa-decimal(16 bits).
*/

#include <stdio.h>

int main() {
    char ch;
    ch = 'A';

    // character array
    char n1[10] = {'h','e','l','l','o','\0'}; 
    char n2[10]; 
    char n3[10];
    char n4[10] = "vaibhav";
    // we can use loop on array of char
    for (int i = 0; i < sizeof(n1); i++)    
        printf("%c \n",n1[i]);    
    
    // gets will take all words till you hit enter
    gets(n2);
    
    // scanf will take only 1st word 
    scanf("%s",n3);

    printf("%s",n4);
    printf("%s %s",n1,n2);    
    printf("%s %s",n1,n3);  
      
    return 0;
} 