#include <stdio.h>
#include <stdbool.h>

int validate(char *name) {
    int i;
    for ( i = 0; name[i] != '\0'; i++) {
        if( !(name[i] > 64 && name[i] < 91) && !(name[i] > 96 && name[i] < 123) 
            && !(name[i] > 47 && name[i] < 59) )
                return 0;
    }
    return 1;
}
int main() {
    char *user_name = "Vaibhav123";
    printf("%s", validate(user_name) ? "true" : "false");;
    
}