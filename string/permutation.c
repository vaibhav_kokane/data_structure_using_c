/* 1.state space tree
    2.Back-Tracking 
    3.Brute-force
    4.Recursion
*/  

#include <stdio.h>

void permutation(char s[],int k) {
    int i;
    static int a[3] = {0};
    static char r[3] = {0};
    if(s[k] == '\0') {
        r[k] == '\0';
        printf("\t %s",r);
    } else {
        for ( i = 0; s[i] != '\0'; i++) {
            if(a[i] == 0){
                r[k] = s[i];
                a[i] = 1;
                permutation(s,k+1);
                a[i] = 0;
            }
        }
    }   
}

void main() {
char str[] = "ABC";
permutation(str,0);
}