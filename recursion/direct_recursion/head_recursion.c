/* Example of Head Recursion */
#include <stdio.h>

void func(int n) {
    if (n > 0) {
        func(n-1);          /* ==> Head recursion */
        printf("%d ",n);
    }
}

void main() {
    func(5);
}