/* To understand Tree Recursion lets first understand Linear Recursion.
 If a recursive function calling itself for one time then its known as Linear Recursion.
  Otherwise if a recursive function calling itself for more than one time then its 
  known as Tree Recursion.
*/

#include <stdio.h>

void fun(int n) {
    if (n > 0) { 
        printf("%d ",n);
        fun(n - 1);    
        fun(n - 1);
    } 
}

void main() {
    fun(4);
}
