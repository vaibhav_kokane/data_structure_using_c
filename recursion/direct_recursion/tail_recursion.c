#include <stdio.h>

void func(int n) {
    if (n > 0) {
        printf("%d ",n);
        func(n - 1);  /* tail recursion */
    }
}

void main() {
    func(5);
}

