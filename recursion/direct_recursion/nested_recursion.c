/* Nested Recursion: Recursion inside recursion (Recursion is taking parameter as recursion)*/

#include <stdio.h>

int func(int n) {
    if (n > 100) {
        return n -10;
    }
    else {
        return func(func(n+11));      
    }
}

void main() {
    int x = func(95);
  printf("%d ", x);
}