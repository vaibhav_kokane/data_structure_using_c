#include <stdio.h>

// method one 
int exponantialOne(int m,int n) {
    static int x;
    if (n == 0 )
        return 1;
    x = exponantialOne(m,n-1) * m;
    // printf("%d ",x);
    return x;
}

// method two
int exponantialTwo(int m, int n) {
    int p;
    if (n == 0)
        return 1;
    if(n % 2 == 0)
        return exponantialTwo(m*m, n/2);
    return m * exponantialTwo(m*m, (n - 1)/2);
}

void main() {
printf("%d ",exponantialOne(2,3));
printf("%d ",exponantialTwo(2,3));
}