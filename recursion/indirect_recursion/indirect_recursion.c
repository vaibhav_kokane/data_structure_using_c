// Indirect recursion (circular way of calling ) funA => funB => funA repeat; 
#include <stdio.h>
 
/* fucntion prototype to avoid warning:implicit declaration of function 'funcB'; */
void funcB(int n);

void funcA(int  n) {
    if (n > 0) {
        printf("%d ",n);
        funcB(n-1);
    }
}

void funcB(int n) {
    if (n > 0) {
        printf("%d ",n);
        funcA(n/2);
    }
}

void main() {
    funcA(100);
}