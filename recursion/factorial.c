/* Factorial of given no using recursion*/
#include <stdio.h>

// function using recursion
int factRecursive(int n) {
    if (n == 0 || n == 1 ) {
        return 1;
    }
    else {
        return factRecursive(n-1) * n;
    }
}

// function using  for loop
int factFor(int n) {
    int fact = 1;
    if(n == 0 || n ==1) 
        return 1;
    for (int i = 2; i <= n; i++) {
        fact = fact * i;
    }
    return fact;
}

// function using  while loop
int factWhile(int n) {
    int fact = 1;
    if(n == 0 || n == 1)
        return 1;
    while(n >= 2) {
        fact = fact * n;
        n--;
    }
    return fact;
}

void main() {
   int n = 0;
    printf("%d ",factRecursive(5));
    printf("%d ",factFor(5));
    printf("%d ",factWhile(5));
}