#include <stdio.h>
#include <stdlib.h>

struct Node {
    struct Node *prev;
    int data;
    struct Node *next;
}*first = NULL,*last = NULL;

void create(int arr[],int n) {
    int i;
    struct Node *t;
    first = (struct Node *)malloc(sizeof(struct Node));
    first->data = arr[0];
    first->prev = first->next = NULL;
    last = first;

    for(i = 1; i < n; i++) {
        t = (struct Node *)malloc(sizeof(struct Node));
        t->data = arr[i];
        t->prev = last;
        t->next = last->next;
        last->next = t;
        last = t;
    }
}

void display(struct Node *p) {
    while(p) {
        printf("%d ",p->data);
        p = p->next;
    }
}

int length(struct Node *p) {
    int len = 0;
    while(p != NULL) {
        len++;
        p = p->next;
    }
    return printf("\nlength is = %d\n",len);
}

int insert(struct Node *p,int data, int pos) {
    struct Node *t; int i;
    if(pos < 0 && pos > length(p))
        return -1;
    else {
        t = (struct Node *)malloc(sizeof(struct Node));
        t->data = data;
        if(first == NULL) {
            t->data = data;
            t->prev = t->next = NULL;
            last = t;
        } else if(pos == 0) {
            t->prev = NULL;
            t->next = first;
            first->prev = t;
            first = t;
        } else {
            for ( i = 0; i < pos-1; i++) p = p->next;
            if(p->next)
                p->next->prev = t;
            t->prev = p;
            t->next = p->next;
            p->next = t;
            
        }
    }
    return 1;
}

int delete(struct Node *p, int index) {
    int i,x;
    if(index > 0 && index <= length(p)) {
        if(index == 1) {
            x = p->data;
            first = first->next;
            free(p);
            if(first) 
                first->prev = NULL;
        } else {
            for ( i = 0; i < index-1; i++) p = p->next;
            x = p->data;
            p->prev->next = p->next;
            if(p->next)          
                p->next->prev = p->prev;
            free(p);
        }
    }
    return printf("deleted value = %d\n",x);
}

void reverse(struct Node *p) {
    struct Node *temp;
    
    while(p) {
        temp = p->next;
        p->next = p->prev;
        p->prev = temp;
        p = p->prev;
        if(p && p->next == NULL)
            first = p;
    }
    display(first);
}

void main() {
    int A[] = {1,2,3,4,5};
    create(A,5);
    display(first);
    // length(first);
    // insert(first,0,5);
    printf("\n");
    // delete(first,1);
    // reverse(first);
    
}