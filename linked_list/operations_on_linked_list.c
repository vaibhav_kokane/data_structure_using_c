#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

// structure of node
struct Node {
    int data;
    struct Node *next;
}*first = NULL,*second = NULL,*third = NULL;

// creation of linked list 1
int create(int *data,int n) {
    struct Node *temp,*last;int *list;
    first = (struct Node *) malloc(sizeof(struct Node));
    first->data = data[0];
    first->next = NULL;
    last = first;
    int i;
    for ( i = 1; i < n; i++) {
        temp = (struct Node *) malloc (sizeof(struct Node));
        temp->data = data[i];
        temp->next = NULL;
        last->next = temp;
        last = temp;
    }
} 

int create1(int *data,int n) {
    struct Node *temp,*last;int *list;
    second = (struct Node *) malloc(sizeof(struct Node));
    second->data = data[0];
    second->next = NULL;
    last = second;
    int i;
    for ( i = 1; i < n; i++) {
        temp = (struct Node *) malloc (sizeof(struct Node));
        temp->data = data[i];
        temp->next = NULL;
        last->next = temp;
        last = temp;
    }
} 

// displaying linked list
void display(struct Node *p) {
    while(p != NULL) {
        printf("%d ",p->data);
        p = p->next;
    }
}

// displying linked list using recursion
void rDisplay(struct Node *p) {
    if(p != NULL){
        printf("%d ",p->data);
        rDisplay(p->next);
    }
}

// displaying linked list in reverse order using recursion
void RevRecDisplay(struct Node *p) {
    if(p != NULL) {
        RevRecDisplay(p->next);
        printf("%d ",p->data);
    }
} 

// length of linked list
int length(struct Node *p) {
    int count = 0;
    while(p != NULL) {
        count++;
        p = p->next;
    }
    return count;
}

// length of linked list using recursion
int recursivelength(struct Node *p) {
    if(p != NULL) return recursivelength(p->next) + 1;
    else return 0;
}

// sum of all elements of linked list
int sum(struct Node *p) {
    int sum = 0;
    while(p) {
        sum += p->data;
        p = p->next;
    }
    return sum;
}

// sum of all elements of linked list 
int recSum(struct Node *p) {
    if(p == 0)
        return 0;
    return recSum(p->next) + p->data;
}

// max in linked list
int max(struct Node *p) {
    int max = INT_MIN;
    while(p != NULL) {
        if(p->data > max) {
            max = p->data;
        }
        p = p->next;
    }
    return max;
}

// max using recursion
int recMax(struct Node *p) {
    int x = 0;
    if(p == 0)
        return INT_MIN;
    else {
        x = recMax(p->next);
        return x > p->data ? x : p->data;
    }
}

// search element in linkedlist
int serach(struct Node *p,int key) {
    while(p != NULL) {
        if(key == p->data) 
            return p->data; 
        p = p->next;
    }
    return 0;
}

// search element in linked list using recursion
int recSearch(struct Node *p,int key) {
    if(p == NULL)
        return 0;
    else if(p->data == key)
        return p->data;
    else
        recSearch(p->next,key);
}

// search and move to head 
struct Node * searchAndMoveToHead(struct Node *p,int key) {
    struct Node *q;
    while(p != NULL) {
        if(p->data == key) {
            // check if its not head element
            if(p != first) {
                q->next = p->next;
                p->next= first;
                first = p;
                return p;
            }
            return p;
        }
        q=p;
        p = p->next;
    }
}

// insert element in linked list
void insert(struct Node *first, bool head, int position, bool tail, int data) {
    struct Node *t,*p;
    t = (struct Node *)malloc(sizeof(struct Node));
    t->data = data;
    if(position < 0 || position > length(first)) {
        printf("invalid position value = %d \n",position);
    }
    else {
            if(first == NULL) {
            t->next = NULL;
            first = t;
            } else {
            if(head) {
                t->next = first;
                first = t;
            } else if(position > 0) {
                if(position > length(first)-1)
                    return;
                int i;
                position -= 1;
                p = first;
                for ( i = 0; (i < position) && (p != NULL); i++) p = p->next;
                if(p) {
                    t->next = p->next;
                    p->next = t;
                }
            } else if (tail) {
                p = first; int i; int count;
                count = length(first);
                for ( i = 0; i < count-1 && p; i++) p = p->next;
                if(p) {
                    t->next = p->next;
                    p->next = t;
                    
                }
            } 
        }
    }
}

// insert element in sorted linked list
void insertInSorted(struct Node *first, int data) {
    struct Node *p,*q,*t;
    t = (struct Node *)malloc(sizeof(struct Node *));
    t->data = data;
    t->next = NULL;
    if(!first) { 
        first = t;
    } else {
        p = first;
        q = NULL;
        while(p->data < data) {
            q = p;
            p = p->next;        
        }
        q->next = t;
        t->next = p;
    }
}

// delete node form linked list
int delete(struct Node *p,int value) {
    struct Node *q; q = NULL; int x;
    
    if(p) {
        while(p != 0) {
            if(p->data == value){
                q->next = p->next;
                x = p->data;
                free(p);
                return x;
            }
            q = p;
            p = p->next;
        }
        return -1;
    }
}

// check whether the linked list sorted or not
int checkIsSorted(struct Node *p) {
    int x = INT_MIN;
    while(p != NULL) {
        if(x > p->data)
            return 0;
        x = p->data;
        p = p->next;
    }
    return 1;
}

//  removing duplicate elements from linked list
void removeDuplicateElements(struct Node *p) {
    struct Node *q;q = p->next;
    while(q != NULL) {
        if(q->data != p->data) {
            p = q;
            q = q->next;
        } else {
            p->next = q->next;
            free(q);
            q = p->next;
        }
    }
}

// reverse linked list
void reverse(struct Node *p) {
    int *A,i = 0;
    A = (int *)malloc(sizeof(int) * length(p));
    struct Node *q = p;
    while(q) {
        A[i] = q->data;
        q = q->next; 
        i++;
    }
    q = p; i--;
    while(q) {
        q->data = A[i];
        q = q->next;
        i--;
    }
}

// reversing using sliding pointer
void reverseSlidingPointer(struct Node *p) {
    struct Node *q,*r; q = r = NULL;
    while(p) {
        r = q;
        q = p;
        p = p->next;
        q->next = r;
    }
    first = q;
}

// recursive reverse 
void reverseRecursive(struct Node *p,struct Node *q) {
  if(p) {
      reverseRecursive(p->next,p);
      p->next = q;
  } else
    first = q;
}

// concat two linked list
void concat(struct Node *p, struct Node *q) {
    third = p;
    while(p->next) 
        p = p->next;
    p->next = q;
}

// merging two link list
void merge(struct Node *first,struct Node *second) {
    struct Node *last;
    if(first->data < second->data) {
        third =  last  = first;
        first = first->next;
        third->next = NULL;
    } else {
        third =  last  = second;
        second = second->next;
        third->next = NULL;
    }

    while(first && second){
        if(first->data < second->data) {
            last->next = first;
            last = first;
            first = first->next;
            last->next  = NULL;
        } else {
            last->next = second;
            last = second;
            second = second->next;
            last->next  = NULL;
        }
    }

    if(first) last->next = first;
    if(second) last->next = second;
}

// checking whether a linkedlist is linear or looped
void isLoop(struct Node *first) {
    struct Node *p,*q;
    p = q = first;

    do {
        p = p->next;
        q = q->next;
        q = q ? q->next : NULL;
    } while(p && q && (p != q));
    printf("is looped = %s",p == q ? "true" : "false");
}

void main() {
    int data[5] = {10,20,30,40,50};
    create(data,5); 
    display(first); printf("\n");
    // int data1[5] = {1,2,3,4,5};
    // create1(data1,5); 
    // display(second); printf("\n\n");
    // rDisplay(first); printf("\n");
    // RevRecDisplay(first); printf("\n");
    // length(first);
    // printf("length of linked list = %d",recursivelength(first)); printf("\n");
    // printf("sum of all element is = %d",sum(first)); printf("\n");
    // printf("sum of all element is = %d",recSum(first)); printf("\n");
    // printf("max no present in linked list = %d",max(first)); printf("\n");
    // printf("max no present in linked list = %d",recMax(first)); printf("\n");
    // printf("key is found = %d",serach(first,5)); printf("\n");
    // printf("key is found = %d",recSearch(first,5)); printf("\n");
    // struct Node *found= searchAndMoveToHead(first,1);
    // printf("Search and move to head key is = %d ",found->next);  printf("\n");
    // display(first);printf("\n");
    // insert(first,false,0,true,6);
    // insert(first,true,0,false,4);
    // insert(first,false,1,false,5);
    // insert(first,false,0,true,7);
    // insert(first,false,-1,false,7);
    // insertInSorted(first,35);
    //  printf("deleted node = %d ",delete(first,50)); printf("\n");
    // int x = checkIsSorted(first);
    // printf("is sorted? = %s",x ? "true":"false"); printf("\n");
    // removeDuplicateElements(first);
    // reverse(first);
    // reverseSlidingPointer(first);
    // reverseRecursive(first,NULL);
    // concat(first,second);
    // merge(first,second);
    // making linked list looped one
    struct Node *t1,*t2;
    t1 = first->next->next;
    t2 = first->next->next->next->next;
    t2->next = t1;
    isLoop(first);
    // display(third); printf("\n");
}