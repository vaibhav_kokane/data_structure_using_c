/* 
    Diagonal Matrix - only diagonal elements are non-zero.
    M[i,j] = 0, if (i != j)
*/ 
#include <stdio.h>

struct Matrix {
    int a[5];
    int n;
};

void set(struct Matrix *m, int i, int j, int x) {
    if(i == j) {
        m->a[i-1] = x;
    }
}

int get(struct Matrix m, int i, int j) {
    if(i==j) 
        return m.a[i-1];
    else 
        return 0;
}

void display(struct Matrix m) {
    int i,j;
    for ( i = 0; i < m.n; i++) {
        for(j = 0; j < m.n; j++) {
            if(i==j)
                printf("%d ",m.a[i]);
            else
                printf("0 ");
        }
        printf("\n");
    }
}

int main() {
    struct Matrix m;
    m.n = 4;
    set(&m,1,1,10);
    set(&m,2,2,10);
    set(&m,3,3,10);
    set(&m,4,4,10);
    set(&m,5,5,10);

    printf("%d \n",get(m,2,2));

    display(m);
    return 0;
}