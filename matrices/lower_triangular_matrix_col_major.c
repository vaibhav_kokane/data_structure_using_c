#include <stdio.h>
#include <stdlib.h>

struct Matrix {
    int *A;
    int n;
};

void set(struct Matrix *m, int i, int j, int x) {
    if(i >= j)
        m->A[m->n*(j-1)+(j-2)*(j-1)/2+(i-j)]=x;
}

int get(struct Matrix m,int i,int j) {
    if(i>=j)
        return m.A[i];
    else
        return 0;
}

void display(struct Matrix m)
{
    int i,j;
    for(i=1;i<=m.n;i++) {
        for(j=1;j<=m.n;j++) {
            if(i>=j)
                printf("%d ",m.A[i]);
            else
                printf("0 ");
        }
    printf("\n");
    }
}

int main() {
    
    struct Matrix m;
    free(m.A);
    printf("Enter the no of matrix size = ");
    scanf("%d",&m.n);
    m.A = (int *) malloc((m.n *(m.n +1))/2 * sizeof(int));
    int i,j,x;
    for ( i = 1; i <= m.n; i++) {
        for ( j = 1; j <= m.n; j++) {
            if(i >= j) {
                scanf("%d",&x);
                set(&m,i,j,x);
            }
        }
    }

    printf("\n");
    printf("get output = %d",get(m,1,1));
    printf("\n");
    display(m);
    return 0;
}