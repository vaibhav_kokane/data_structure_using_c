#include <stdio.h>
#include <stdlib.h>

struct Term {
    int coef;
    int expo;
};

struct Poly {
    int n;
    struct Term *terms;
};

void create(struct Poly *p) {
    int i;
    printf("Enter the no of terms = ");
    scanf("%d",&p->n);
    p->terms = (struct Term *) malloc(p->n * sizeof(struct Term));
    for ( i = 0; i < p->n; i++) {
        scanf("%d%d",&p->terms[i].coef,&p->terms[i].expo);
    }
}

void display(struct Poly *p) {
    int i;
    for ( i = 0; i < p->n; i++) {
        printf("%dx%d+",p->terms[i].coef,p->terms[i].expo); 
    }
    printf("\n");
}

struct Poly * add(struct Poly p1, struct Poly p2) {
    printf("Addition \n");
    struct Poly *sum;
    sum = (struct Poly *) malloc(sizeof(struct Poly));
    sum->n = p1.n + p2.n;
    sum->terms = (struct Term *) malloc(sum->n * sizeof(struct Term));
    int i,j,k;
    i = j = k = 0;
    while (i < p1.n && j < p2.n ) {
        if(p1.terms[i].expo > p2.terms[j].expo) {
            sum->terms[k++] = p1.terms[i++];
        } else if(p2.terms[j].expo > p1.terms[i].expo) {
            sum->terms[k++] = p2.terms[j++];
        } else {
            sum->terms[k].expo = p1.terms[i].expo;
            sum->terms[k++].coef = p1.terms[i++].coef + p2.terms[j++].coef;
        }
    }   

    for (; i < p1.n; i++) sum->terms[i] = p1.terms[i] ;
    for (; j < p2.n; j++) sum->terms[j] = p2.terms[j];  

    sum->n = k;
    return sum;   
}

void main() {
    struct Poly p1,p2,*p3;
    create(&p1);
    create(&p2);
    p3 = add(p1,p2);
    display(&p1);
    display(&p2);
    display(p3);
}