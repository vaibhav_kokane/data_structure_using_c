/*
    lower triangular matrix is type of matrix in which diagonally lower section of matrix 
    values are not-zero's. 
*/ 

#include <stdio.h>
#include <stdlib.h>

struct Matrix {
    int *A;
    int n;
};

void set(struct Matrix *m, int i, int j, int x) {
    if(i >= j)
        m->A[(i*(i-1))/2 + (j-1)] = x;
}

int get(struct Matrix m, int i, int j) {
    if(i >= j)
        printf("%d \n",m.A[i-1]);
    else 
        return 0;
}

void display(struct Matrix m) {
    int i,j;
    for (i = 0; i < m.n ; i++) {
        for (j = 0; j < m.n ; j++) {
            if(i >= j )
                printf("%d ",m.A[i]);     
            else
                printf("0 ");
        }
        printf("\n");
    }
}

int main() {
    
    struct Matrix m;
    free(m.A);
    printf("Enter the no of matrix size = ");
    scanf("%d",&m.n);
    m.A = (int *) malloc((m.n *(m.n +1))/2 * sizeof(int));

    set(&m,1,1,1); set(&m,2,1,1); set(&m,2,2,1); set(&m,3,1,1); set(&m,3,2,1);
    set(&m,3,3,1); set(&m,4,1,1); set(&m,4,2,1); set(&m,4,3,1); set(&m,4,4,1);
    set(&m,5,1,1); set(&m,5,2,1); set(&m,5,3,1); set(&m,5,4,1); set(&m,5,5,1);
    
    // get(m,1,1);
    display(m);
    return 0;
}