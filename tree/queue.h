#ifndef queue.h
#define queue.h

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Node {
    struct Node *lChild;
    int data;
    struct Node *rChild;
};

struct Queue {
    int size;
    // here front and rear are index pointer and not the address pointers.
    int front;
    int rear;
    struct Node **Q;
};

void create(struct Queue *q,int size) {
    q->size = size;
    q->front = q->rear = 0;
    q->Q = (struct Node **)malloc(size * sizeof(struct Node));
}

int isFull(struct Queue *q) {
    if((q->rear+1)%(q->size) == q->front) return true;
    return false;
}

void enqueue(struct Queue *q, struct Node *data) {
    if(isFull(q)) printf("Circular queue is Full!\n");
    else {
        q->rear = (q->rear + 1) % q->size;
        q->Q[q->rear] = data;
    } 
}

int isEmpty(struct Queue *q) {
    if(q->front == q->rear) return true;
    return false;
}

struct Node * dequeue(struct Queue *q) {
    struct  Node * x = NULL;
    if(isEmpty(q)) printf("Circular queue is empty!\n");
    else {
        q->front = (q->front + 1) % q->size;
        x = q->Q[q->front];
    }
    return x;
}

#endif
