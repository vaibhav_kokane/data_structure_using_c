#ifndef stack.h
#define stack.h

#include <stdio.h>
#include <stdlib.h>

// structure of stack
struct Stack {
    int size;
    int top;
    struct Node **s;
};

// create a stack
void createStack(struct Stack *st,int size) {
    st->size = size;
    st->top = -1;
    st->s = (struct Node **)malloc(st->size * sizeof(struct Node));
}

// check whether  a stack is overflow
int isFullStack(struct Stack st) {
    if(st.top >= st.size-1)
        return 1;
    else
        return 0;
}

// check whether a stack is underflow
int isEmptyStack(struct Stack *st) {
    if(st->top == -1)
        return 1;
    else 
        return 0;
}

// push element inside stack
void push(struct Stack *st, struct Node *x) {
    if(st->top == st->size-1)
        printf("Stack overflow! \n");
    else {
        st->top++;
        st->s[st->top] = x;
    }
}

// pop out element form stack
struct Node *pop(struct Stack *st) {
    struct Node *x = NULL;
    if(st->top == -1)
        printf("stack is underflow!");
    else { 
        x = st->s[st->top--];
    }
    return x;
}

#endif
