#include "queue.h"
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Node *root = NULL;

// creating  a tree
void createTree() {
    struct Node *t, *p;
    struct Queue q;
    int x;
    create(&q,10);

    printf("Enter root value  = ");
    scanf("%d",&x);
    root = (struct Node *)malloc(sizeof(struct Node));
    root->data = x;
    root->lChild = root->lChild = 0;
    enqueue(&q,root);

    while(!isEmpty(&q)) {
        p = dequeue(&q);
        printf("Enter left child %d = ",p->data);
        scanf("%d",&x);
        if(x != -1) {
            t = (struct Node *)malloc(sizeof(struct Node));
            t->data = x;
            t->lChild = t->rChild = NULL;
            p->lChild = t;
            enqueue(&q,t);
        }

        printf("Enter right child %d = ",p->data);
        scanf("%d",&x);
        if(x != -1) {
            t = (struct Node *)malloc(sizeof(struct Node));
            t->data = x;
            t->lChild = t->rChild = NULL;
            p->rChild = t;
            enqueue(&q,t);
        }
    }
}

// Recursive pre-order traversal
void preOrder(struct Node *p) {
    if(p) {
        printf("%d ",p->data);
        preOrder(p->lChild);
        preOrder(p->rChild);
    }
}

// Recursive in-order traversal
void inOrder(struct Node *p) {
    if(p) {
        inOrder(p->lChild);
        printf("%d ",p->data);
        inOrder(p->rChild);
    }
}

// Recursive post-order traversal
void postOrder(struct Node *p) {
    if(p) {
        postOrder(p->lChild);
        postOrder(p->rChild);
        printf("%d ",p->data);
    }
}

// Iterative pre-order traversal
void preOrderIterative(struct Node *p) {
    struct Stack st;
    int size = 20;
    createStack(&st,size);
    while(p || !(isEmptyStack(&st))) {
        if(p) {
            printf("%d ",p->data);
            push(&st,p);
            p = p->lChild;
        } else {
            p = pop(&st);
            p = p->rChild;
        }
    }
}

// Iterative in-order traversal
void inOrderIterative(struct Node *p) {
    struct Stack st;
    int size = 20;
    createStack(&st,size);
    while(p || !(isEmptyStack(&st))) {
        if(p) {
            push(&st,p);
            p = p->lChild;
        } else {
            p = pop(&st);
            printf("%d ",p->data);
            p = p->rChild;
        }
    }
}

// level order traversal or breadth level traversal
void levelOrderTraversal(struct Node *p) {
    struct Queue Q;
    create(&Q,20);
    printf("%d ",p->data);
    enqueue(&Q,p);

    while(!isEmpty(&Q)) {
        p = dequeue(&Q);
        if(p->lChild) {
            printf("%d ",p->lChild->data);
            enqueue(&Q,p->lChild);
        }
        if(p->rChild) {
            printf("%d ",p->rChild->data);
            enqueue(&Q,p->rChild);
        }
    }
}


// total count nodes in a binary tree 
int count(struct Node *p) {
    int x,y;
    x = y = 0;
    if(p) {
        x = count(p->lChild);
        y = count(p->rChild);
        return x + y + 1;
    }
    return 0;
}

// count only leaf/terminal node
int countLeafNodes(struct Node *p) {
    if(p == NULL)
        return 0;
    if(!(p->lChild) && !(p->rChild))
        return countLeafNodes(p->lChild) + countLeafNodes(p->rChild) + 1;
    return countLeafNodes(p->lChild) + countLeafNodes(p->rChild);
}

int countNonLeafNodes(struct Node *p) {
     int x,y;
    x = y = 0;
    if(p) {
        x = countNonLeafNodes(p->lChild);
        y = countNonLeafNodes(p->rChild);
        if((p->lChild != NULL) || (p->rChild != NULL))
            return x + y + 1;
        else
            return x + y;
    }
    return 0;
}

// height of binary tree
int height(struct Node *p) {
    int x, y;
    x = y = 0;
    if(p == NULL) return 0;
    x = height(p->lChild);
    y = height(p->rChild);
    if(x > y) 
        return x + 1;
    return y + 1;
}

// sum of all tree node data
int sum(struct Node *p) {
    int x,y;
    x = y = 0;
    if(p) {
        x = sum(p->lChild);
        y = sum(p->rChild);
        return x + y + p->data;
    }
    return 0;
}

int main() {
    createTree();
    printf("\n");printf("\n");
    printf("Recursive Traversals = \n");
    printf("Pre-order : ");
    preOrder(root); printf("\n");
    printf("In-order : ");
    inOrder(root); printf("\n");
    printf("Post-order : ");
    postOrder(root); printf("\n\n");
   
    printf("Iterative Traversals = \n");
    printf("Pre-order : ");
    preOrderIterative(root); printf("\n");
    printf("In-order : ");
    inOrderIterative(root); printf("\n\n");

    printf("Level-order Traversal or Breadth-order Traversal\n");
    levelOrderTraversal(root); printf("\n\n");

    printf("Total nodes in a tree = %d\n",count(root));
    printf("Height of a tree = %d\n",height(root));
    printf("Sum of a  all tree node's data = %d\n",sum(root));
    printf("Leaf/Terminal nodes in a tree = %d\n",countLeafNodes(root));
    printf("Non-Leaf/Non-Terminal nodes in a tree = %d\n",countNonLeafNodes(root));


}