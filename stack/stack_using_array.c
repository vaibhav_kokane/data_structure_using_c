#include <stdio.h>
#include <stdlib.h>

// structure of stack
struct Stack {
    int size;
    int top;
    int *s;
};

// create a stack
void create(struct Stack *st) {
    printf("Enter the size of stack = \n");
    scanf("%d",&st->size);
    st->top = -1;
    st->s = (int *)malloc(sizeof(int) * st->size);
    
}

// display all element
void display(struct Stack st) {
    int i;
    if(st.top >= 0) {
        for( i = st.top; i >= 0; i--) {
        printf("%d ",st.s[i]);
       }
    }
}

// check whether  a stack is overflow
int isFull(struct Stack st) {
    if(st.top >= st.size-1)
        return 1;
    else
        return 0;
}

// check whether a stack is underflow
int isEmpty(struct Stack st) {
    if(st.top == -1)
        return 1;
    else 
        return 0;
}

// check the top of the stack
int topStack(struct Stack st) {
    if(!isEmpty(st))
        return st.s[st.top];
    return 0;
}

// push element inside stack
void push(struct Stack *st, int x) {
    if(st->top == st->size-1)
        printf("Stack overflow! \n");
    else {
        st->top++;
        st->s[st->top] = x;
    }
}

// pop out element form stack
int pop(struct Stack *st) {
    int x;
    if(st->top == -1)
        printf("stack is underflow!");
    else { 
        x = st->s[st->top--];
        printf("poped element = %d\n",x);
    }
}

// peek a element from stack 
int peek(struct Stack st,int index) {
    int x = -1;
    if(st.top-index+1 < 0)
        return -1;
    else
        return x = st.s[st.top - index+1]; 
}

void main() {
struct Stack st;

create(&st);
push(&st,10);
push(&st,20);
push(&st,30);
push(&st,40);
push(&st,50);
printf("is full = %s\n",isFull(st) == 1 ? "True" : "False");
printf("stack top = %d\n",topStack(st));
pop(&st);
pop(&st);
pop(&st);
pop(&st);
pop(&st);
printf("peeked element = %d\n",peek(st,1));
printf("is empty= %s\n",isEmpty(st) == 1 ? "True" : "False");
display(st);
}