#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

struct Stack {
    int data;
    struct Stack *next;
};

// global variable
struct Stack *top = NULL;

// push
void push(struct Stack *st,int x) {
    struct Stack *t;
    t = (struct Stack *)malloc(sizeof(struct Stack));
    if(t == NULL)
        printf("stack is overflow!");
    else {
        t->data = x;
        t->next = top;
        top = t;
    }
}

// pop
void pop(struct Stack *st) {
    struct Stack *p;
    if(!top)
        printf("stack is underflow! ");
    else {
        p = top;
        int x = p->data;
        top = p->next;
        free(p);
    }
}

// display
void display(struct Stack st) {
    if(!top)
        printf("stack is empty.");
    else
    {
        int i; struct Stack *p;
        p = top;
        while(p) {
            printf("%d ",p->data);
            p = p->next;
        }
        printf("\n");
    }
    
}

// is empty ?
bool isEmpty() {
    if(!top)
        return true;
    return false;
}

// is full ?
bool isFull() {
    struct Stack *t = (struct Stack *)malloc(sizeof(struct Stack));
    if(!t)
        return true;
    return false;
}

// stack top 
int stackTop() {
    if(!top)
        printf("Stack is empty!");
    else {
        return top->data;
    }
} 


void main() {
    struct Stack st,*top = NULL;
    push(&st,10);
    push(&st,20);
    push(&st,30);
    display(st);
    pop(&st);
    display(st);
    printf("%s\n",isEmpty() ? "true" : "false");
    printf("%s\n",isFull() ? "true" : "false");
    printf("%d",stackTop());


}