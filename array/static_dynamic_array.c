#include <stdio.h>

// static array
int staticArray[5] = {1, 2, 3, 4, 5};
/* Note : Size of array is optional when declaring and initializing array at once. 
          The C compiler automatically determines array size using number of array elements. 
          Hence, you can write above array initialization as.
*/
int staticArray1[] = {1, 2, 3, 4, 5};

// dynamic array
int dynamicArray[5];

void main() {
    dynamicArray[0] = 1;
    dynamicArray[1] = 2;
    dynamicArray[2] = 3;
    dynamicArray[3] = 4;
    dynamicArray[4] = 5;

    printf("static staticArray[0]= %d  dyanamicArray[0] = %d",staticArray[0],dynamicArray[0]);
}