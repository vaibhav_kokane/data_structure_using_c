#include <stdio.h>
#include <stdlib.h>
#define len(arr) ((int) (sizeof (arr) / sizeof (arr)[0]))

/* Note : You must explicitly provide row and column size. 
          Otherwise C compiler will generate compilation errors.
          Array column size is optional if specifying individual rows within pair of curly braces.
*/

/* Array column size is optional if specifying individual rows within pair of curly braces */
int twoDiArray[5][10] = { {1,2,3,4,5,6,7,8,9,10},
                   {1,2,3,4,5,6,7,8,9,10},
                   {1,2,3,4,5,6,7,8,9,10},
                   {1,2,3,4,5,6,7,8,9,10},
                   {1,2,3,4,5,6,7,8,9,10} 
            };

void traverseTwoDiamensionArray() {
    for (int row = 0; row < len(twoDiArray); row++) {
        for (int col = 0; col < len(twoDiArray[0]); col++) {
            printf("%d ",twoDiArray[row][col]);
        }
        printf("\n");
    }
}

void main() {
    traverseTwoDiamensionArray();
}