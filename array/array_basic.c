#include <stdio.h>
#define len(arr) ((int)(sizeof(arr)/sizeof(arr)[0]))

int a[5];
int A[5] = {0};
int B[ ] = {};
int C[5] = {1,2};
int D[5] = {1,2,3,4,5};


void traverse() {
    for (int i = 0; i < len(D); i++) {
        printf("\n\n");
        printf("traversing array : ");
        printf("D[%d] = %d \t",i,D[i]);
        printf("*(D + %d) = %d \n",i,*(D + i));
        printf("address in memory : ");
        printf("&D[%d] = %u ",i,&D[i]);
    }
}

int main() {
    traverse();
}