#include <stdio.h>
#include <stdlib.h>

// declared pointer p and q
int *p,*q;
void increseSizeOfArray() {
    // pointer p pointing to an array of size 5 which is in heap memory.
    p = (int *)malloc(5 * sizeof(int)) ;
    p[0] = 10; p[1] = 20; p[2] = 30; p[3] = 40; p[4] = 50;

    // pointer 1 pointing to an array of size 10 which is in heap memory.
    q = (int *)malloc(10 * sizeof(int));

    // copying value from p to q
    for (int i = 0; i < 5; i++)
        q[i] = p[i];
    
    // deallocating  heap memory
    free(p);
    
    // giving address of q to p
    p = q;
    
    // deleting q (pointing to no one now)
    q = NULL;

    // thus we incresed the size of array p
    for (int i = 0; i < 10;i ++) {
        printf("%d ",p[i]);
    }
}

int main() {
    increseSizeOfArray();
}