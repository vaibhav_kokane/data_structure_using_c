#include <stdio.h>
#include <stdlib.h>
#define len(arr) ((int) (sizeof(arr)/sizeof(arr)[0]))

void main() {
    static int n;
    int *A[4]; /* array of pointer will be in stack */
    int **B; /*pointer will be stack*/
    
    // this will be in heap
    printf("enter size of col in array = ");
    scanf("%d",&n);
    A[0] = (int *) malloc(n*sizeof(int));
    A[1] = (int *) malloc(n*sizeof(int));
    A[2] = (int *) malloc(n*sizeof(int));
    A[3] = (int *) malloc(n*sizeof(int));
    A[4] = (int *) malloc(n*sizeof(int));

    // this will be heap
    /* array of pointer will be in heap */
    B = (int **) malloc(n*sizeof(int));
    // this will be in heap
    B[0] = (int *) malloc(n*sizeof(int));
    B[1] = (int *) malloc(n*sizeof(int));
    B[2] = (int *) malloc(n*sizeof(int));
    B[3] = (int *) malloc(n*sizeof(int));
    B[4] = (int *) malloc(n*sizeof(int));

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < 5; col++) {
        //  A[row][col] = row + col;   
         scanf("%d",&A[row][col]);
        }   
    }

    for (int row = 0; row < 4; row++) {
        for (int col = 0; col < n; col++) {
        //  A[row][col] = row + col;   
         printf("%d ",A[row][col]);
        }   
        printf("\n");
    }

    printf("\n\n");

    for (int row = 0; row < n; row++) {
        for (int col = 0; col < n; col++) {
        //  A[row][col] = row + col;   
         scanf("%d",&B[row][col]);
        }   
    }
    
    for (int row = 0; row < n; row++) {
        for (int col = 0; col < n; col++) {
        //  B[row][col] = row + col;   
         printf("%d ",*(*(B + row) + col));
        }   
        printf("\n");
    }  
}