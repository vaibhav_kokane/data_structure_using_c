#include <stdio.h>
#include <stdlib.h>

struct Array {
    int a[10];
    int size;
    int length;
};

// display
void display(struct Array *arrZ) {
    for (int i = 0; i < arrZ->length; i++)
    {
        printf("%d ",arrZ->a[i]);
    }
    
}

// Union A ∪ B  on unsorted array using while loop
struct Array* unionOperation(struct Array *arrX, struct Array *arrY) {
    int i, j, k, q, m = arrX->length, n = arrY->length; i = j = k = 0;
    struct Array *arrZ = (struct Array *)malloc(sizeof(struct Array));

    while(i < m && j < n) {
        if(arrX->a[i] < arrY->a[j])
            arrZ->a[k++] = arrX->a[i++];
        else if(arrY->a[j] < arrX->a[i]) {
            arrZ->a[k++] = arrX->a[j++];
        } else {
            arrZ->a[k++] = arrX->a[i++];
            j++;
        }
    }

    for(;i < m; i++) {
        arrZ->a[k++] = arrX->a[i];
    }
    
    for(;j < n; j++) {
        arrZ->a[k++] = arrY->a[j];
    }
    arrZ->length = k;
    arrZ->size = 10;

    return arrZ;
}

// Intersection A ∩ B
struct Array* interSectionOperation(struct Array *arrX, struct Array *arrY) {
    int i, j, k, q, m = arrX->length, n = arrY->length; i = j = k = 0;
    struct Array *arrZ = (struct Array *)malloc(sizeof(struct Array));

  while(i < m && j < n) {
        if(arrX->a[i] < arrY->a[j])
            i++;
        else if(arrY->a[j] < arrX->a[i])
            j++;
        else if(arrX->a[i] == arrY->a[j]) {
            arrZ->a[k++] = arrX->a[i++];
            j++;
        }
    }

    arrZ->length = k;
    arrZ->size = 10;

    return arrZ;
}

// Difference A - B
struct Array* differenceOperation(struct Array *arrX, struct Array *arrY) {
    int i, j, k, q, m = arrX->length, n = arrY->length; i = j = k = 0;
    struct Array *arrZ = (struct Array *)malloc(sizeof(struct Array));
    
    while(i < m && j < n) {
        if(arrX->a[i] < arrY->a[j])
            arrZ->a[k++] = arrX->a[i++];
        else if(arrY->a[j] < arrX->a[i]) {
            j++;
        } else {
            i++; j++;
        }
    }

    for(;i < m; i++) {
        arrZ->a[k++] = arrX->a[i];
    }
    
    arrZ->length = k;
    arrZ->size = 10;

    return arrZ;
}

int main() {
    struct Array arrX = {{1,2,3,4,5,7},10,6};
    struct Array arrY = {{7,8,9,10,15,20},10,6};
    // display(unionOperation(&arrX,&arrY));
    // display(interSectionOperation(&arrX,&arrY));
    display(differenceOperation(&arrX,&arrY));
    return 0;
}